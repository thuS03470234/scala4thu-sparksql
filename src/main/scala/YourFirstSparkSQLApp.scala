import org.apache.spark.SparkConf
import org.apache.spark.sql.{DataFrame, SparkSession}

/**
  * Created by mark on 04/06/2017.
  */
object YourFirstSparkSQLApp extends App{

  val ss = SparkSession
    .builder()
    .master("local[*]")
    .appName("Spark SQL first example")
    .getOrCreate()

  val src: DataFrame =ss.read.option("header", "true")
    .csv("dataset/small-ratings.csv")   //資料是dataset裡的small-ratings.csv

  val src1: DataFrame =ss.read.option("header", "true")
    .csv("dataset/movies.csv")   //資料是dataset裡的movies.csv


  src.createOrReplaceTempView("ratings")
  src1.createOrReplaceTempView("movies")

  //貼sqlite語法，基本
  ss.sql("SELECT * From ratings").show()
  // 符合格式|userId|movieId|rating|timestamp|
  ss.sql("SELECT * FROM ratings ORDER BY timestamp DESC").show()
  // 符合格式|movieId|mean|
  ss.sql("SELECT movieId,avg(rating) AS mean FROM ratings GROUP BY movieId").show()
  // 符合格式|movieId|min|max|
  ss.sql("SELECT movieId,min(rating) AS min,max(rating) AS max FROM ratings GROUP BY movieId").show()
  // 配合App(格式要符合老師的)-> |movieId|mean|，降序
  ss.sql("SELECT movieId,avg(rating) AS mean FROM ratings GROUP BY movieId ORDER BY mean DESC").show()
  // 配合App(格式要符合老師的)-> |userId|movieId|rating| timestamp|，找出movieId為30707且userId=107799
  ss.sql("SELECT * FROM ratings WHERE movieId=30707 AND userId=107799").show()
  // 配合App(格式要符合老師的)-> |movieId|title|rating|timestamp|，從small-ratings.csv及movies.csv兩張表Join起來，以timestamp降序排列
  ss.sql("SELECT r.movieId,m.title,r.rating,r.timestamp FROM ratings AS r JOIN movies AS m WHERE r.movieId=m.movieId ORDER BY r.timestamp DESC").show()




  ss.sql("SELECT * From movies").show()   //貼sqlite語法

  // 配合App(格式要符合老師的)-> |movieId|title|，從movie.csv中，找出movieId為10的資料
  ss.sql("SELECT movieId,title FROM movies where movieId=10").show()


  // 配合App(格式要符合老師的)-> |movieId|title|rating|timestamp|，從small-ratings.csv及movies.csv兩張表Join起來，以timestamp降序排列
  ss.sql("SELECT r.movieId,m.title,r.rating,r.timestamp FROM ratings AS r JOIN movies AS m WHERE r.movieId=m.movieId ORDER BY r.timestamp DESC").show()
}



// sqlite 設定
// view -> tool windows -> database -> +(加資料 ) -> data source -> sqlite -> ...找資料 -> 專案裡dataset中的檔案（.sqlite）-> download -> test -> ok
// 不小心關掉 -> movie右鍵 -> open console